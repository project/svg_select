<?php


namespace Drupal\svg_select\Element;


use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Select;


/**
 * Provides a form element for a drop-down menu or scrolling selection box.
 *
 * @FormElement("svg_select")
 */
class SVGSelect extends Select {

  /**
   * Returns the element properties for this element.
   *
   * @return array
   *   An array of element properties. See
   *   \Drupal\Core\Render\ElementInfoManagerInterface::getInfo() for
   *   documentation of the standard properties of all elements, and the
   *   return value format.
   */
  public function getInfo() {
    $class = get_class($this);
    $parentClass = get_parent_class($this);
    return [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#process' => [
        [$class, 'processSVG'],
        [$parentClass, 'processSelect'],
        [$parentClass, 'processAjaxForm'],
      ],
      '#pre_render' => [
        [$parentClass, 'preRenderSelect'],
      ],
      '#theme' => 'svg_select',
      '#theme_wrappers' => ['form_element'],
      '#options' => [],
    ];
  }

  public static function processSVG(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#attached']['library'][] = 'svg_select/base';
    return $element;
  }

}
