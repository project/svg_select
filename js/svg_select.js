(function ($, Drupal) {
  var findKey = function (value, options) {
    for (var key in options) {
      if (options.hasOwnProperty(key) && options[key] === value) {
        return key;
      }
    }
  };

  var getClassFromSVG = function ($element) {
    if ($element.attr('class').baseVal) {
      return $element.attr('class').baseVal;
    }
    else {
      return $element.attr('class');
    }
  };

  var setClassOnSVG = function ($element, className) {
    if ($element.attr('class').baseVal) {
      $element.attr('class').baseVal = className;
    }
    else {
      $element.attr('class', className);
    }
  };

  Drupal.behaviors.svgSelect = {
    attach: function (context, settings) {
      $('svg + select').once('svg-select').each(function () {
        var $select = $(this);
        var baseClass = getClassFromSVG($('[data-term]', context));
        var options = {};

        $select.find('option').each(function () {
          options[this.value] = this.textContent;
        });

        var multiple = $select.attr('multiple');

        $('svg', context).click(function () {
          $select.val(null).change();
        });

        $('[data-term]', context).click(function (e) {
          e.stopPropagation();
          if (multiple) {
            var values = $select.val() || [];
            var newValue = findKey($(this).data('term'), options);
            var index = values.indexOf(newValue);

            if (index > -1) {
              delete values[index];
            }
            else {
              values.push(newValue);
            }

            $select.val(values).change();
          }
          else {
            $select.val(findKey($(this).data('term'), options)).change();
          }
        });

        $select.change(function (e) {
          $('[data-term]', context).each(function () {
            var term_name = $(this).data('term');
            var available = term_name in options;
            if (available === true) {
              setClassOnSVG($(this), baseClass);
            }
            else {
              setClassOnSVG($(this), baseClass + ' is-unavailable');
            }
          });

          if (multiple) {
            var selected = $(this).val() || [];
          }
          else {
            var selected = [$(this).val()];
          }

          if ($('.js-form-type-svg-select > ul').length === 0) {
            $('.js-form-type-svg-select').append($('<ul class="map-selection"></ul>'));
          }

          $('.js-form-type-svg-select > ul').empty();

          for (var i = 0; i < selected.length; i++) {
            $('[data-term="' + options[selected[i]] + '"]').each(function () {
              setClassOnSVG($(this), baseClass + ' is-selected');
            });

            if (i < 4) {
              $('.js-form-type-svg-select > ul', context).append($('<li>' + options[selected[i]] + '</li>'));
            }
            else if (i === 4) {
              $('.js-form-type-svg-select > ul', context).append($('<li>…</li>'));
            }

          }
        }).change();
      })
    }
  };
})(jQuery, Drupal);
